/* eslint-disable camelcase */
import { env } from '../config/envConfig';

const merge = require('deepmerge');

const enUsers = {
  preferences: {
    login: 'WHATA_testing1',
    password: 'KF6_32P2rfThhAE',
    contactable: true,
    contactableMethods: {
      mail: false,
      sms: true,
      call: false,
    },
  },
  statements: { login: 'WHATA_autotest7', password: 'AutoTest1234' },
  gambling: { login: 'WHATA_gambltst', password: 'YR_G4uGN48T' },
  transactions: { login: 'WHATA_testing1', password: 'KF6_32P2rfThhAE' },
  noFundsAccount: { login: 'lolo89', password: 'Password1234' },
  depositLimits: { login: 'esOxi192329635', password: 'B3nchmark' },
  myBets: { login: 'WHATA_lukasz07', password: 'pass123' },
  quickDeposit: { login: 'comOx212404765', password: 'B3nchmark' },
};

const esUsers = merge(enUsers, {
  transactions: { login: 'spitfire_es04', password: 'Qwertyui9' },
  myBets: { login: 'spitfire_es04', password: 'Qwertyui9' },
});

const itUsers = merge(enUsers, {
  transactions: { login: 'johnybravo', password: 'tester1234' },
  myBets: { login: 'johnybravo', password: 'tester1234' },
});

const enLivUsers = merge(enUsers, {
  transactions: { login: 'WHATA_V_ma', password: 'pa55word2015' },
  myBets: { login: 'WHATA_autotest7', password: 'AutoTest1234' },
  quickDeposit: { login: 'WHATA_G_ma_d', password: 'pa55word2015' },
  livePerson: { login: 'WHATA_LivePer10', password: 'B3nchmark' },
  deposit: { login: 'LC_TEST_VALID2', password: 'qwerty67' },
});
const enPP2Users = merge(enUsers, {
  transactions: { login: 'pp2_portal_qa0', password: 'password1234' },
  myBets: { login: 'pp2_portal_qa1', password: 'password1234' },
  quickDeposit: { login: 'pp2_portal_qa12', password: 'password1234' },
});
const esLivUsers = merge(enUsers, {
  transactions: { login: 'WHATA_WHGPES1', password: 'B3nchmark' },
  myBets: { login: 'WHATA_WHGPES1', password: 'B3nchmark' },
});

const itLivUsers = merge(enUsers, {
  transactions: { login: 'romagb1', password: 'Tester55' },
  myBets: { login: 'romagb1', password: 'Tester55' },
});
const itPP1Users = merge(enUsers, {
  transactions: { login: 'itOx215152616', password: 'B3nchmark' },
  myBets: { login: 'itOx215152616', password: 'B3nchmark' },
});

const alchemyliv = merge(enUsers, {
  transactions: { login: 'WHATA_V_ma', password: 'pa55word2015' },
  myBets: { login: 'WHATA_M_bw', password: 'pa55word2015' },
  quickDeposit: { login: 'WHATA_G_ma_d', password: 'pa55word2015' },
  deposit: { login: 'LC_TEST_VALID2', password: 'qwerty67' },
  livePerson: { login: 'WHATA_LivePer$$', password: 'B3nchmark' },
});

const allUsers = {
  pp1: enUsers,
  'pp1-es': esUsers,
  'pp1-it': itUsers,
  liv: enLivUsers,
  'liv-es': esLivUsers,
  'liv-it': itLivUsers,
  alchemy_liv: alchemyliv,
  alchemy_pp2: enPP2Users,
  alchemy_pp1_it: itPP1Users,
};

export const users = allUsers[env];
