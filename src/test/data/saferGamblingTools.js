export default Object.freeze({
  SaferGamblingMenus: [
    'Self Assessment',
    'Deposit Limits',
    'Gaming Time Reminders',
    'Time Out',
    'Account Closure',
    'Self Exclusion',
  ],
});
