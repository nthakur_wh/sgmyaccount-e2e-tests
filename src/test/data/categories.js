export default Object.freeze({
  sportBookTiles: [
    'Withdraw',
    'Deposit',
    'My Transactions',
    'My Bets',
    'Safer Gambling',
    'Poker Balance Transfer',
    'Promotions',
    'Account Preferences',
  ],

  vegasTiles: [
    'Withdraw',
    'Deposit',
    'My Transactions',
    'Poker Balance Transfer',
    'Safer Gambling',
    'Account Preferences',
  ],
  alchemyITTiles: ['Scommesse', 'Deposita', 'Preleva', 'Annulla i Prelievi', 'Le mie Transazioni', 'Il Mio Conto'],
  balanceInformationTiles: ['Cash Balance', 'Poker Cash Balance', 'Gaming', 'Sports Free Bets'],

  balanceInformationHelperTiles: [
    'Can be used anywhere except poker',
    'To be used on WH Poker only',
    'Bonus money for Gaming',
    'Free Bet money for Sports betting',
  ],
});
