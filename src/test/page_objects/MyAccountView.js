import Page from './Page';
import { PersonalDetailsView } from './PersonalDetailsView';

export class MyAccountView extends Page {
  get accountHistoryElement() {
    return $('div[data-test-handler=TileAccountHistory]');
  }

  get personalDetailsElement() {
    return $('div[data-test-handler=TilePersonalDetails]');
  }

  get securitySettingsElement() {
    return $('div[data-test-handler=TileLoginSecuritySettings]');
  }

  get siteSettingsElement() {
    return $('div[data-test-handler=TileSiteSettings]');
  }

  get loginSecuritySettingsElement() {
    return $('div[data-test-handler=TileLoginSecuritySettings]');
  }

  get contactTabElement() {
    return $('[data-test-handler="personalDetails.tabs.contact"]');
  }
  get marketingTabElement() {
    return $('[data-test-handler="personalDetails.tabs.marketing"]');
  }

  get emailTextBox() {
    return $('[name="email"]');
  }

  get street1TextBox() {
    return $('[id="street1"]');
  }
  get street2TextBox() {
    return $('[id="street2"]');
  }

  get cityTextBox() {
    return $('[name="city"]');
  }

  get countyTextBox() {
    return $('[id="county"]');
  }

  get postCodeTextBox() {
    return $('[id="postcode"]');
  }

  get mobileNumberTextBox() {
    return $('[name="mobile"]');
  }

  get updateMyDetailsButton() {
    return $('[data-test-handler="submitpersonalDetailsContactButton"]');
  }

  get timeZoneDropDown() {
    return $('[id="timezone"]');
  }

  get timeZoneButton() {
    return $('[data-test-handler="submitpersonalDetailsTimezoneButton"]');
  }

  get marketingSwitch() {
    return $('[data-test-handler="cp-ma-personal-details-marketing-switch-wrapper__switch"]');
  }

  get marketingContactMethod() {
    return $$('[data-test-id="Checkbox"]');
  }

  get marketingDisclaimer() {
    return $('.cp-ma-personal-details-marketing-disclaimer__note');
  }

  get loginSecuritySettingDetailsElement() {
    return $('[data-test-handler="TileLoginSecuritySettings"]');
  }

  get currentPasswordTextBox() {
    return $('[name="currentPassword"]');
  }
  get newPasswordTextBox() {
    return $('[name="newPassword"]');
  }

  get savePasswordButton() {
    return $('[data-test-handler="savePasswordButton"]');
  }

  goToPersonalDetails() {
    this.personalDetailsElement.waitForDisplayed();
    this.personalDetailsElement.click();
    return new PersonalDetailsView();
  }
  goToLoginSecuritySettingsDetails() {
    this.loginSecuritySettingDetailsElement.waitForDisplayed();
    this.loginSecuritySettingDetailsElement.click();
  }
}

const myAccountView = new MyAccountView();
export default myAccountView;
