import Page from './Page';

export class MyBetsView extends Page {
  get openBetsTab() {
    return browser.isMobile ? $('[data-test-handler="myBetsOpen"]') : $('#betslip-my-bets-open-tab');
  }

  get settledBetsTab() {
    return browser.isMobile ? $('[data-test-handler="myBetsSettled"]') : $('#betslip-my-bets-settled-tab');
  }

  get transactionComponent() {
    return browser.isMobile ? $('.cp-ma-transactions-transaction') : $('div.bs-open-bet-summary__header');
  }

  get transactionComponents() {
    return browser.isMobile ? $$('.cp-ma-transactions-transaction') : $$('div.bs-open-bet-summary__header');
  }

  waitForVisible() {
    this.openBetsTab.waitForDisplayed();
    this.settledBetsTab.waitForDisplayed();
    return this;
  }

  showOpenBets() {
    this.waitForVisible();
    this.openBetsTab.click();
    return this;
  }

  showSettledBets() {
    this.waitForVisible();
    this.settledBetsTab.click();
    return this;
  }

  waitForTransactions() {
    this.transactionComponent.waitForDisplayed();
  }
}

const myBetsView = new MyBetsView();
export default myBetsView;
