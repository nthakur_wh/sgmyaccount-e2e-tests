import Page from './Page';
import navView from './NavigationView';

export class Header extends Page {
  get balanceButton() {
    return $('button.cp-ma-myaccount-dropdown-button');
  }

  get backButton() {
    return $('svg.cp-ma-header-button-back_arrow');
  }

  get closeButton() {
    return $('div.cp-ma-header-header__close-button');
  }

  get depositButton() {
    return $('button.action-deposit__button');
  }

  get paymentMethodsMenu() {
    return $('#paymentMethodsMenu');
  }

  get paymentMethodsContent() {
    return $('#paymentMethodsContent');
  }

  get rightNav() {
    return $('.rightNav');
  }

  get rightNavMobile() {
    return $('(//div[@id="WH_help_section"])[1]');
  }

  get liveChat() {
    return $('.chatHelp');
  }
  get liveChatMobile() {
    return $('(//div[@class="chat_help"])[1]');
  }

  get depositIframe() {
    return '[class="deposit-modal__iframe"]';
  }

  get addMethodButton() {
    return $('.blueBtn');
  }

  get paymentActiveButton() {
    return $('//li[contains(@id,"paymentACTIVE_CC_VD")]');
  }

  get bonusLandingPageTile() {
    return $$('.cp-ma-balance-bonus-landing-page-tile__main-text');
  }

  get bonusLandingPageHelper() {
    return $$('.cp-ma-balance-bonus-landing-page-tile__helper-text');
  }

  get bonusLandingPageAmount() {
    return $$('.cp-ma-balance-bonus-landing-page-tile__amount');
  }

  get viewPromotionsButton() {
    return $('.cp-ma-balance-bonus-landing-page-footer button span span');
  }

  get bonusLandingPageFooterText() {
    return $('.cp-ma-balance-bonus-landing-page-footer p');
  }
  get promotionsLandingPage() {
    return $('[data-test-id="promotion-item-image"]');
  }
  get balanceInformationLink() {
    return $('.cp-ma-balance-balance-and-bonus__balance-info-link');
  }

  get bonusLandingPage() {
    return $('.cp-ma-balance-bonus-landing-page');
  }

  get quickDepositLandingPage() {
    return $('.qd.opened.qd-vertical');
  }

  get quickDepositCardNumber() {
    return $('.masked');
  }

  get quickDepositAmount() {
    return $('[name="amount"]');
  }

  get quickDepositAmountButton() {
    return $$('[data-test-id="ButtonGroup.cell"]');
  }

  get quickDepositCVV() {
    return $('[name="cvv"]');
  }

  get quickDepositMoreMethodButton() {
    return $('.qd__other-methods');
  }

  get quickDepositCloseButton() {
    return $('.qd__close-button');
  }

  get quickDepositAmount10Click() {
    return $('(//div[@data-test-id="ButtonGroup.cell"])[1]');
  }
  get quickDepositAmount20Click() {
    return $('(//div[@data-test-id="ButtonGroup.cell"])[2]');
  }
  get quickDepositAmount100Click() {
    return $('(//div[@data-test-id="ButtonGroup.cell"])[3]');
  }
  get helpAndFeedbackClick() {
    return $('//*[contains(@data-test-id,"sub-nav-end-item_help-and-support")]');
  }
  get liveChatClick() {
    return $('[id="live-chat"]');
  }

  get messageUsClick() {
    return $('.start-chat-button.btn.btn-live-person');
  }
  get livePersonChatValidate() {
    return $('[aria-label="Chat window"]');
  }

  get chatInput() {
    return $('[data-lp-point="chat_input"]');
  }

  get sendButtonImage() {
    return $('[data-lp-point="send_button_image"]');
  }

  get chipsSliderItems() {
    return $('.lpc_message__text_agent');
  }

  get closeLPChat() {
    return $('(//*[@alt="End conversation"]/..)[2]');
  }

  get lpConfirmButton() {
    return $('[data-lp-point="confirm_button"]');
  }

  get submitButton() {
    return $('[data-lp-point="submit_button"]');
  }

  get lpMinimizedViewController() {
    return $('[id = "LP_MinimizedViewController_1"]');
  }

  get notificationAlert() {
    return $('[data-lp-point="notification_text"]');
  }

  get lpMinimized() {
    return $('[data-lp-cust-id="minimized"]');
  }

  get contactUs() {
    return $('//*[contains(text(),"Contact Us")]');
  }

  get categoryItem() {
    return $('(//div[@data-testid="category-item"])[last()]');
  }

  get MyAccountLanding() {
    return $('.cp-ma-container-container');
  }

  get cashBalance() {
    return $('.cp-ma-balance-balance-and-bonus__cash-balance');
  }

  get freeBets() {
    return $('.cp-ma-balance-balance-and-bonus__additional-balance');
  }

  get cashBalanceText() {
    return $('//*[@class="cp-ma-balance-balance-and-bonus__cash-balance"]/span[2]');
  }

  get freeBetsGiftBox() {
    return $('//*[@class="cp-ma-balance-balance-and-bonus__additional-balance-value"]/img');
  }
  get myAccountHeader() {
    return $('.cp-ma-container-view__header');
  }

  openMyAccount() {
    this.balanceButton.waitForDisplayed();
    this.balanceButton.click();
    navView.logoutButton.waitForDisplayed();
    this.container.waitForExist();
    this.container.waitForDisplayed();
    return navView;
  }
  closeMyAccountContainer() {
    this.myAccountHeader.waitForDisplayed();
    this.closeButton.waitForDisplayed({ timeout: 5000 });
    browser.pause(1000); // Temp fix Alchemy IT is bit slow
    this.closeButton.click();
    if (!browser.isIOS) {
      this.closeButton.waitForDisplayed({ reverse: true });
      this.container.waitForExist({ reverse: true });
    }
    return this;
  }
  switchToDepositLimitsFrame() {
    $(this.depositIframe).waitForDisplayed();
    browser.switchToFrame($(this.depositIframe));
  }
  enterTextInLivePerson(text) {
    this.chatInput.waitForDisplayed();
    this.chatInput.setValue(text);
    this.sendButtonImage.waitForDisplayed();
    this.sendButtonImage.click();
  }

  closeLivePersonChat() {
    this.closeLPChat.waitForDisplayed();
    this.closeLPChat.click();
    this.lpConfirmButton.waitForDisplayed();
    this.lpConfirmButton.click();
    if (this.submitButton.isDisplayed()) {
      this.submitButton.waitForDisplayed();
      this.submitButton.click();
    }
  }

  clickOnMessageUSButton() {
    this.messageUsClick.waitForDisplayed();
    this.messageUsClick.click();
    browser.pause(5000); // Temporary solution working on this to fix
    if (this.submitButton.isDisplayed()) {
      this.submitButton.click();
    }
    this.livePersonChatValidate.waitForDisplayed();
  }
  balanceInformationTiles() {
    return this.bonusLandingPageTile.map((x) => x.getText());
  }
  balanceInformationHelperTiles() {
    return this.bonusLandingPageHelper.map((x) => x.getText());
  }
}
const header = new Header();
export default header;
