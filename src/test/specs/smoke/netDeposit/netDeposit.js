import navView from '../../../page_objects/NavigationView';
import header from '../../../page_objects/Header';
import accountHistoryView from '../../../page_objects/AccountHistoryView';
import integratedHeader from '../../../page_objects/IntegratedHeader';
import { users } from '../../../common/users';
import tags from 'mocha-tags';

tags('desktkop', 'mobile').describe('05. Account Net Deposit', () => {
  before(() => {
    header.open();
    integratedHeader.loginUser(users.transactions.login, users.transactions.password);
  });

  beforeEach(() => {
    integratedHeader.openMyAccountContainer();
    navView.goToAccountHistoryView();
    accountHistoryView.netDepositInformationButton.click();
  });

  afterEach(() => {
    header.closeMyAccountContainer();
  });

  describe('Net Deposit Information tests', () => {
    it('should display Net Deposit Information', () => {
      expect(accountHistoryView.netDepositTitle.isExisting()).to.equal(true);
    });

    it('Net deposit amount should be displayed', () => {
      accountHistoryView.netDepositAmountButton.click();
      accountHistoryView.netDepositAmountTitle.waitForDisplayed();
      expect(accountHistoryView.netDepositAmountTitle.isExisting()).to.equal(true);
    });
  });
});
