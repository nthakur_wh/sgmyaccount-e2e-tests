/*global expectWdio*/
import header from '../../../page_objects/Header';
import navView from '../../../page_objects/NavigationView';
import integratedHeader from '../../../page_objects/IntegratedHeader';
import { users } from '../../../common/users';
import tags from 'mocha-tags';
import myAccountView from '../../../page_objects/MyAccountView';

import personalDetails from '../../../page_objects/PersonalDetailsView';

tags('desktop', 'mobile').describe('06. Account preferences - landing page', () => {
  describe('Personal Details', () => {
    before(() => {
      header.open();
      integratedHeader.loginUser(users.transactions.login, users.transactions.password);
    });
    beforeEach(() => {
      integratedHeader.openMyAccountContainer();
    });

    afterEach(() => {
      header.closeMyAccountContainer();
    });
    it('should display account preferences landing page', () => {
      navView.goToMyAccountView();
      myAccountView.personalDetailsElement.waitForDisplayed();
      expectWdio(myAccountView.personalDetailsElement).toBeDisplayed();
      expectWdio(myAccountView.loginSecuritySettingsElement).toBeDisplayed();
    });

    it('should display Personal Details landing page', () => {
      navView.goToMyAccountView();
      myAccountView.goToPersonalDetails();
      expectWdio(myAccountView.contactTabElement).toBeDisplayed();
      expectWdio(myAccountView.marketingTabElement).toBeDisplayed();
    });
    it('should display items in contact page ', () => {
      navView.goToMyAccountView();
      myAccountView.goToPersonalDetails();
      expectWdio(myAccountView.emailTextBox).toBeDisplayed();
      expectWdio(myAccountView.street1TextBox).toBeDisplayed();
      expectWdio(myAccountView.street2TextBox).toBeDisplayed();
      myAccountView.cityTextBox.scrollIntoView();
      expectWdio(myAccountView.cityTextBox).toBeDisplayed();
      myAccountView.countyTextBox.scrollIntoView();
      expectWdio(myAccountView.countyTextBox).toBeDisplayed();
      myAccountView.postCodeTextBox.scrollIntoView();
      expectWdio(myAccountView.postCodeTextBox).toBeDisplayed();
      myAccountView.mobileNumberTextBox.scrollIntoView();
      expectWdio(myAccountView.mobileNumberTextBox).toBeDisplayed();
      myAccountView.updateMyDetailsButton.scrollIntoView();
      expectWdio(myAccountView.updateMyDetailsButton).toBeDisplayed();
      myAccountView.timeZoneDropDown.scrollIntoView();
      expectWdio(myAccountView.timeZoneDropDown).toBeDisplayed();
      myAccountView.timeZoneButton.scrollIntoView();
      expectWdio(myAccountView.timeZoneButton).toBeDisplayed();
    });

    it('should display items in marketing page ', () => {
      navView.goToMyAccountView();
      myAccountView.goToPersonalDetails();
      myAccountView.marketingTabElement.click();
      expectWdio(myAccountView.marketingSwitch).toBeDisplayed();
      expectWdio(myAccountView.marketingDisclaimer).toBeDisplayed();
      expect(myAccountView.marketingContactMethod.length).to.equal(3);
    });

    it('should switch off the toggle when I unselect the last marketing option', () => {
      navView.goToMyAccountView();
      myAccountView.goToPersonalDetails();
      myAccountView.marketingTabElement.click();
      personalDetails.selectMarketingToggle();
      personalDetails.deselectMarketingEmail();
      expect(personalDetails.isMarketingEmailSelected()).to.equal(false);
      expect(personalDetails.isMarketingSmsSelected()).to.equal(true);
      expect(personalDetails.isMarketingPhoneSelected()).to.equal(true);
      expect(personalDetails.isMarkertingToggleSelected()).to.equal(true);

      personalDetails.deselectMarketingSms();
      expect(personalDetails.isMarketingEmailSelected()).to.equal(false);
      expect(personalDetails.isMarketingSmsSelected()).to.equal(false);
      expect(personalDetails.isMarketingPhoneSelected()).to.equal(true);
      expect(personalDetails.isMarkertingToggleSelected()).to.equal(true);

      personalDetails.deselectMarketingPhone();
      expect(personalDetails.isMarketingEmailSelected()).to.equal(false);
      expect(personalDetails.isMarketingSmsSelected()).to.equal(false);
      expect(personalDetails.isMarketingPhoneSelected()).to.equal(false);
      expect(personalDetails.isMarkertingToggleSelected()).to.equal(false);
    });
    it('should switch off all marketing options', () => {
      navView.goToMyAccountView();
      myAccountView.goToPersonalDetails();
      myAccountView.marketingTabElement.click();
      expect(personalDetails.isMarketingEmailSelected()).to.equal(false);
      expect(personalDetails.isMarketingSmsSelected()).to.equal(false);
      expect(personalDetails.isMarketingPhoneSelected()).to.equal(false);
      expect(personalDetails.isMarkertingToggleSelected()).to.equal(false);
    });

    it('should switch on all marketing options', () => {
      navView.goToMyAccountView();
      myAccountView.goToPersonalDetails();
      myAccountView.marketingTabElement.click();
      personalDetails.selectMarketingToggle();
      expect(personalDetails.isMarketingEmailSelected()).to.equal(true);
      expect(personalDetails.isMarketingSmsSelected()).to.equal(true);
      expect(personalDetails.isMarketingPhoneSelected()).to.equal(true);
      expect(personalDetails.isMarkertingToggleSelected()).to.equal(true);
    });

    it('should display all validation errors if user sends the empty form', () => {
      navView.goToMyAccountView();
      myAccountView.goToPersonalDetails();
      personalDetails.submitContactForm();
      expectWdio(personalDetails.contactEmailValidationError).toBeDisplayed();
      expectWdio(personalDetails.contactStreet1ValidationError).toBeDisplayed();
      expectWdio(personalDetails.contactCityValidationError).toBeDisplayed();
      expectWdio(personalDetails.contactPostcodeValidationError).toBeDisplayed();
      expectWdio(personalDetails.contactMobileValidationError).toBeDisplayed();
    });
    it('should send contact form successfully', () => {
      navView.goToMyAccountView();
      myAccountView.goToPersonalDetails();
      personalDetails.fillInContactForm();
      personalDetails.submitContactForm();

      expectWdio(personalDetails.contactSuccessMessageText).toBeDisplayed();
    });
  });

  describe('Login & Security Settings', () => {
    beforeEach(() => {
      integratedHeader.openMyAccountContainer();
    });

    afterEach(() => {
      header.closeMyAccountContainer();
    });
    it('should display Login & Security Settings landing page and validate content', () => {
      navView.goToMyAccountView();
      myAccountView.goToLoginSecuritySettingsDetails();
      expectWdio(myAccountView.currentPasswordTextBox).toBeDisplayed();
      expectWdio(myAccountView.newPasswordTextBox).toBeDisplayed();
      expectWdio(myAccountView.savePasswordButton).toBeDisplayed();
    });
    it('should land to my account after click on back button', () => {
      navView.goToMyAccountView();
      myAccountView.goToLoginSecuritySettingsDetails();
      header.backButton.click();
      navView.myAccountButton.waitForDisplayed();
      expectWdio(navView.myAccountButton).toBeDisplayed();
    });
  });
});
