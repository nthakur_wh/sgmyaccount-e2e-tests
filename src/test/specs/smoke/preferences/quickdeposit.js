/*global expectWdio*/
import header from '../../../page_objects/Header';
import integratedHeader from '../../../page_objects/IntegratedHeader';
import { users } from '../../../common/users';
describe('12. Quick Deposit Limits view', () => {
  before(() => {
    header.open();
    integratedHeader.loginUser(users.quickDeposit.login, users.quickDeposit.password);
    header.depositButton.click();
    header.quickDepositLandingPage.waitForDisplayed();
  });
  it('should display quick deposit limits landing page', () => {
    expectWdio(header.quickDepositLandingPage).toBeDisplayed();
  });
  it('should display card number', () => {
    expect(header.quickDepositCardNumber.getText()).contain('****');
  });
  it('should display default amount £ 10 ', () => {
    expect(header.quickDepositAmount.getValue()).contain('10.00');
  });
  it('should display 3 quick deposit amount button', () => {
    expect(header.quickDepositAmountButton.length).equal(3);
  });
  it('should display quick deposit CVV text box', () => {
    expectWdio(header.quickDepositCVV).toBeDisplayed();
  });
  it('should display quick deposit more method button', () => {
    expectWdio(header.quickDepositMoreMethodButton).toBeDisplayed();
  });
  it('should display quick deposit close button', () => {
    expectWdio(header.quickDepositCloseButton).toBeDisplayed();
  });
  it('should increase  quick deposit amount after click on +10 , +20 ,+100', () => {
    header.quickDepositAmount10Click.click();
    expect(header.quickDepositAmount.getValue()).contain(browser.getUrl().includes('vegas') ? '20.00' : '11.00');
    header.quickDepositAmount20Click.click();
    expect(header.quickDepositAmount.getValue()).contain(browser.getUrl().includes('vegas') ? '40.00' : '16.00');
    header.quickDepositAmount100Click.click();
    expect(header.quickDepositAmount.getValue()).contain(browser.getUrl().includes('vegas') ? '140.00' : '26.00');
  });
});
