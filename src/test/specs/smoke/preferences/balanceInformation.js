/*global expectWdio*/
import header from '../../../page_objects/Header';
import integratedHeader from '../../../page_objects/IntegratedHeader';
import { users } from '../../../common/users';
import categories from '../../../data/categories';
import navView from '../../../page_objects/NavigationView';
if (!process.env.ENV.match('alchemy_pp1_it')) {
  describe('07. Balance Information', () => {
    before(() => {
      header.open();
      integratedHeader.loginUser(users.transactions.login, users.transactions.password);
      integratedHeader.openMyAccountContainer();
    });
    it('should display balance information link', () => {
      expectWdio(header.balanceInformationLink).toBeDisplayed();
    });

    it('should display bonus landing page', () => {
      header.balanceInformationLink.waitForDisplayed({ timeout: 5000 });
      header.balanceInformationLink.click();
      header.bonusLandingPage.waitForDisplayed({ timeout: 5000 });
      expectWdio(header.bonusLandingPage).toBeDisplayed();
    });

    it('should display bonus landing page tile', () => {
      expect(header.bonusLandingPageTile.length).equal(4);
    });

    it('should display bonus landing page helper', () => {
      expect(header.bonusLandingPageHelper.length).equal(4);
    });

    it('should display bonus landing page Amount', () => {
      expect(header.bonusLandingPageAmount.length).equal(4);
    });

    it('should display View promotions button', () => {
      expectWdio(header.viewPromotionsButton).toBeDisplayed();
    });
    it('should display all balance information menu', () => {
      const actualTiersNames = header.balanceInformationTiles();
      const ExpectedTiersHeaders = categories.balanceInformationTiles;
      expect(
        actualTiersNames,
        'Actual tiers headers names or order are not equal to expected one'
      ).to.have.same.ordered.members(ExpectedTiersHeaders);
    });

    it('should display all balance information helper menu', () => {
      const actualTiersNames = header.balanceInformationHelperTiles();
      const ExpectedTiersHeaders = categories.balanceInformationHelperTiles;
      expect(
        actualTiersNames,
        'Actual tiers headers names or order are not equal to expected one'
      ).to.have.same.ordered.members(ExpectedTiersHeaders);
    });

    it('should display landing page footer text', () => {
      const actualTiersNames = header.bonusLandingPageFooterText.getText();
      expect(actualTiersNames).to.have.string(
        browser.getUrl().includes('sports')
          ? 'Check out our promotions to see what other bonuses are available'
          : 'Check out our promotions to see what other gaming bonuses are available'
      );
    });

    it('should display view promotions landing page', () => {
      header.viewPromotionsButton.click();
      navView.switchToWindowHandle();
      header.promotionsLandingPage.waitForDisplayed({ timeout: 5000 });
      expect(browser.getUrl()).contains('/promotions');
    });
  });
}
