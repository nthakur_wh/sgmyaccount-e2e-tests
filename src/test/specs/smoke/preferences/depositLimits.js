/*global expectWdio*/
import header from '../../../page_objects/Header';
import integratedHeader from '../../../page_objects/IntegratedHeader';
import { users } from '../../../common/users';
describe('09. Deposit Limits view', () => {
  before(() => {
    header.open();
    integratedHeader.loginUser(users.deposit.login, users.deposit.password);
  });
  it('should display Deposit Limits landing page', () => {
    header.depositButton.click();
    header.switchToDepositLimitsFrame();
    if (browser.isMobile) {
      header.addMethodButton.waitForDisplayed({ timeout: 5000 });
      header.addMethodButton.click();
    }
    expectWdio(header.paymentMethodsMenu).toBeDisplayed();
  });
  it('should display Deposit Limits payment Methods Content', () => {
    if (browser.isMobile) {
      header.paymentActiveButton.waitForDisplayed({ timeout: 5000 });
      header.paymentActiveButton.click();
    }
    expectWdio(header.paymentMethodsContent).toBeDisplayed();
  });
  it('should display Deposit Limits need help content', () => {
    expectWdio(browser.isMobile ? header.rightNavMobile : header.rightNav).toBeDisplayed();
  });
  it('should display Deposit Limits live chat content', () => {
    expectWdio(browser.isMobile ? header.liveChatMobile : header.liveChat).toBeDisplayed();
  });
});
