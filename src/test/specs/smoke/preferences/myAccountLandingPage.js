/*global expectWdio*/
import header from '../../../page_objects/Header';
import integratedHeader from '../../../page_objects/IntegratedHeader';
import { users } from '../../../common/users';
import tags from 'mocha-tags';
import navView from '../../../page_objects/NavigationView';
import categories from '../../../data/categories.js';
tags('desktop', 'mobile').describe('19. My Account - landing page', () => {
  before(() => {
    header.open();
    integratedHeader.loginUser(users.transactions.login, users.transactions.password);
    integratedHeader.openMyAccountContainer();
  });
  after(() => {
    header.closeMyAccountContainer();
  });
  it('should display my account page', () => {
    expectWdio(header.MyAccountLanding).toBeDisplayed();
  });

  it('should display cash balance and free bets', () => {
    expectWdio(header.cashBalance).toBeDisplayed();
    if (!process.env.ENV.match('alchemy_pp1_it')) {
      expectWdio(header.freeBets).toBeDisplayed();
      expectWdio(header.freeBetsGiftBox).toBeDisplayed();
    }
    expect(header.cashBalanceText.getText()).contain(!process.env.ENV.match('alchemy_pp1_it') ? '£' : '€');
  });

  it('should display balance information link', () => {
    expectWdio(header.balanceInformationLink).toBeDisplayed();
  });

  it('should display user name and last login details', () => {
    expect(navView.welcomeMessage.getText().length).is.greaterThan(0);
    expect(navView.lastLoginDetails.getText().length).is.greaterThan(0);
  });

  it('should display footer link, Feedback, Help and Logout', () => {
    expectWdio(navView.footerFeedback).toBeDisplayed();
    expectWdio(navView.footerHelp).toBeDisplayed();
    expectWdio(navView.footerFeedback).toBeDisplayed();
    expectWdio(navView.logoutButton).toBeDisplayed();
  });

  it('should display all menu tiles', () => {
    if (process.env.ENV.match('alchemy_pp1_it')) {
      expect(navView.countAllNavigationTiles()).to.equal(6);
    } else {
      expect(navView.myAccountMenuTiles.length).equal(browser.getUrl().includes('vegas') ? 6 : 8);
    }
  });

  it('should display account number', () => {
    expect(navView.logoutButton.getText().length).is.greaterThan(0);
  });
  it('should display all the menu', () => {
    const actualTiersNames = navView.getTiersNames();
    const sbExpectedTiersHeaders = categories.sportBookTiles;
    const vegasExpectedTiersHeaders = categories.vegasTiles;
    const alchemyITExpectedTiersHeaders = categories.alchemyITTiles;

    if (!process.env.ENV.match('alchemy_pp1_it')) {
      expect(
        actualTiersNames,
        'Actual tiers headers names or order are not equal to expected one'
      ).to.have.same.ordered.members(
        browser.getUrl().includes('vegas') ? vegasExpectedTiersHeaders : sbExpectedTiersHeaders
      );
    } else {
      expect(
        actualTiersNames,
        'Actual tiers headers names or order are not equal to expected one'
      ).to.have.same.ordered.members(alchemyITExpectedTiersHeaders);
    }
  });
});
