/*global expectWdio*/
import header from '../../../page_objects/Header';
import integratedHeader from '../../../page_objects/IntegratedHeader';
import { users } from '../../../common/users';
import navView from '../../../page_objects/NavigationView';
if (!(process.env.ENV.endsWith('es') || process.env.ENV.endsWith('it'))) {
  describe('01. Live Person', () => {
    before(() => {
      header.open();
      const livePersonUser = users.livePerson.login;
      const randomLength = Math.floor(Math.random() * 30);
      integratedHeader.loginUser(livePersonUser.replace('$$', randomLength), users.livePerson.password);
    });

    beforeEach(() => {
      if (browser.getUrl().includes('vegas')) {
        browser.refresh();
        browser.pause(5000);
        browser.maximizeWindow();
        header.categoryItem.scrollIntoView();
        header.contactUs.scrollIntoView();
        header.contactUs.click();
      } else {
        header.helpAndFeedbackClick.waitForDisplayed();
        header.helpAndFeedbackClick.click();
        header.liveChatClick.waitForDisplayed({ timeout: 5000 });
        browser.pause(300);
        header.liveChatClick.click();
      }
    });
    it('should redirect to "app/ask#" page', () => {
      const parentWindow = browser.getWindowHandle();
      navView.switchToWindowHandle();
      const currentUrl = browser.getUrl();
      header.messageUsClick.waitForDisplayed();
      expect(currentUrl).contains('/app/ask');
      browser.closeWindow();
      browser.switchToWindow(parentWindow);
    });

    it('wait for "Message US" button to display', () => {
      const parentWindow = browser.getWindowHandle();
      navView.switchToWindowHandle();
      header.messageUsClick.waitForDisplayed();
      expectWdio(header.messageUsClick).toBeDisplayed();
      browser.closeWindow();
      browser.switchToWindow(parentWindow);
    });

    it('launch live person chat', () => {
      const parentWindow = browser.getWindowHandle();
      navView.switchToWindowHandle();
      header.clickOnMessageUSButton();
      expectWdio(header.livePersonChatValidate).toBeDisplayed();
      header.closeLPChat.click();
      browser.closeWindow();
      browser.switchToWindow(parentWindow);
    });

    it('enter text in live person chat ', () => {
      const parentWindow = browser.getWindowHandle();
      navView.switchToWindowHandle();
      header.clickOnMessageUSButton();
      header.enterTextInLivePerson('This is LP Testing');
      header.chipsSliderItems.waitForDisplayed();
      expectWdio(header.chipsSliderItems).toBeDisplayed();
      header.closeLivePersonChat();
      browser.closeWindow();
      browser.switchToWindow(parentWindow);
    });

    it('badge notification alert ', () => {
      const parentWindow = browser.getWindowHandle();
      navView.switchToWindowHandle();
      header.clickOnMessageUSButton();
      header.enterTextInLivePerson('This is LP Testing Closing Plz ignore');
      header.chipsSliderItems.waitForDisplayed();
      browser.refresh();
      header.notificationAlert.waitForDisplayed();
      expect(header.notificationAlert.isDisplayed()).to.be.true;
      header.lpMinimized.waitForDisplayed();
      header.lpMinimized.click();
      header.closeLivePersonChat();
      browser.closeWindow();
      browser.switchToWindow(parentWindow);
    });
    if (!process.env.ENV.startsWith('alchemy')) {
      it('validate chat to move SB page without refresh', () => {
        const parentWindow = browser.getWindowHandle();
        navView.switchToWindowHandle();
        header.clickOnMessageUSButton();
        header.enterTextInLivePerson('This is LP Testing');
        header.chipsSliderItems.waitForDisplayed();
        browser.switchToWindow(parentWindow);
        header.livePersonChatValidate.waitForDisplayed();
        expectWdio(header.livePersonChatValidate).toBeDisplayed();
        header.lpMinimizedViewController.waitForDisplayed();
        header.lpMinimizedViewController.click();
        header.closeLivePersonChat();
      });
    }

    it('close live person chat', () => {
      navView.switchToWindowHandle();
      header.clickOnMessageUSButton();
      header.enterTextInLivePerson('This is LP Testing Closing Plz ignore');
      header.chipsSliderItems.waitForDisplayed();
      header.closeLivePersonChat();
      header.livePersonChatValidate.waitForDisplayed();
      expect(header.livePersonChatValidate.isDisplayed()).to.be.false;
      browser.closeWindow();
    });
  });
}
