/*global expectWdio*/
import header from '../../../page_objects/Header';
import tags from 'mocha-tags';
import navView from '../../../page_objects/NavigationView';
if (!(process.env.ENV.endsWith('es') || process.env.ENV.endsWith('it'))) {
  tags('desktkop', 'mobile').describe('02. Live Person UnAuth Chat', () => {
    before(() => {
      header.open();
    });

    beforeEach(() => {
      if (browser.getUrl().includes('vegas')) {
        header.categoryItem.scrollIntoView();
        header.contactUs.scrollIntoView();
        header.contactUs.click();
      } else {
        header.helpAndFeedbackClick.click();
        header.liveChatClick.waitForDisplayed();
        header.liveChatClick.click();
      }
    });
    it('should redirect to "app/ask#" page', () => {
      const parentWindow = browser.getWindowHandle();
      navView.switchToWindowHandle();
      const currentUrl = browser.getUrl();
      header.messageUsClick.waitForDisplayed();
      expect(currentUrl).contains('/app/ask');
      browser.closeWindow();
      browser.switchToWindow(parentWindow);
    });

    it('wait for "Message US" button to display', () => {
      const parentWindow = browser.getWindowHandle();
      navView.switchToWindowHandle();
      header.messageUsClick.waitForDisplayed();
      expectWdio(header.messageUsClick).toBeDisplayed();
      browser.closeWindow();
      browser.switchToWindow(parentWindow);
    });

    it('launch live person chat', () => {
      const parentWindow = browser.getWindowHandle();
      navView.switchToWindowHandle();
      header.clickOnMessageUSButton();
      expectWdio(header.livePersonChatValidate).toBeDisplayed();
      header.closeLPChat.click();
      browser.closeWindow();
      browser.switchToWindow(parentWindow);
    });

    it('enter text in live person chat ', () => {
      const parentWindow = browser.getWindowHandle();
      navView.switchToWindowHandle();
      header.clickOnMessageUSButton();
      header.enterTextInLivePerson('This is LP Testing');
      header.chipsSliderItems.waitForDisplayed();
      expectWdio(header.chipsSliderItems).toBeDisplayed();
      header.closeLivePersonChat();
      browser.closeWindow();
      browser.switchToWindow(parentWindow);
    });
    it('close live person chat', () => {
      navView.switchToWindowHandle();
      header.clickOnMessageUSButton();
      header.enterTextInLivePerson('This is LP Testing Closing Plz ignore');
      header.chipsSliderItems.waitForDisplayed();
      header.closeLivePersonChat();
      browser.pause(500);
      expect(header.livePersonChatValidate.isDisplayed()).to.be.false;
      browser.closeWindow();
    });
  });
}
